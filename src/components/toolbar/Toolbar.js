import React from 'react';
import './toolbar.css';

export const Toolbar = () => {

  const renderButtonShareScreen = () => (
    <div className="d-flex mr-3 flex-column align-items-center justify-content-between">
      <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M21 6C22.6569 6 24 4.65685 24 3C24 1.34315 22.6569 0 21 0C19.3431 0 18 1.34315 18 3C18 4.65685 19.3431 6 21 6ZM5 2H16.1C16.0344 2.32311 16 2.65753 16 3C16 3.34247 16.0344 3.67689 16.1 4H5C3.89543 4 3 4.89543 3 6V14C3 15.1046 3.89543 16 5 16H19C20.1046 16 21 15.1046 21 14V8C21.7111 8 22.3875 7.85155 23 7.58396V14C23 16.2091 21.2091 18 19 18H13V20H16V22H8V20H11V18H5C2.79086 18 1 16.2091 1 14V6C1 3.79086 2.79086 2 5 2Z"
              style={{fill: "#666", transform: "scale(1.5)"}}/>
      </svg>
      <a>Share Screen</a>
    </div>
  )

const renderButtonMessage = () => (
  <div className="d-flex mr-3 flex-column align-items-center justify-content-between">
    <svg width="40" height="40" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M23.2998 11.8H21.4138C18.8608 11.8 16.3998 14.56 16.3998 16.768V21L9.4998 27.9V21H4.8998C2.3698 21 0.299805 18.93 0.299805 16.4V4.90005C0.299805 2.37005 2.3698 0.300049 4.8998 0.300049H18.6998C21.2298 0.300049 23.2998 2.37005 23.2998 4.90005V11.8ZM23.2998 14.1H37.0998C39.6298 14.1 41.6998 16.17 41.6998 18.7V30.2C41.6998 32.73 39.6298 34.8 37.0998 34.8H32.4998V41.7L25.5998 34.8H23.2998C20.7698 34.8 18.6998 32.73 18.6998 30.2V18.7C18.6998 16.17 20.7698 14.1 23.2998 14.1Z" fill="#515151"/>
    </svg>
    <a>Message</a>
  </div>
)

  const renderButtonQuestion = () => (
    <div className="d-flex flex-column align-items-center justify-content-between">
      <svg width="32" height="40" viewBox="0 0 34 42" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1.22703 26.0179L1.09484 11.6921C1.08846 11.0013 1.38129 10.3361 1.90891 9.84284C2.43652 9.34959 3.1557 9.06869 3.90823 9.06193V9.06193C4.66077 9.05518 5.38501 9.32312 5.92164 9.80682C6.45827 10.2905 6.76333 10.9503 6.7697 11.6412L6.85383 20.7576" stroke="#515151" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M6.85428 20.8307L6.72059 6.34392C6.71415 5.64535 7.01887 4.9726 7.56772 4.47367C8.11657 3.97474 8.86459 3.6905 9.64722 3.68347C10.4299 3.67645 11.183 3.94721 11.741 4.43621C12.2989 4.9252 12.616 5.59237 12.6225 6.29094L12.744 19.4607" stroke="#515151" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M18.4187 19.4054L18.3093 7.55317C18.3029 6.85945 18.6077 6.19135 19.1565 5.69585C19.7054 5.20035 20.4534 4.91804 21.2361 4.91101V4.91101C22.0187 4.90398 22.7718 5.17283 23.3298 5.65839C23.8877 6.14396 24.2047 6.80647 24.2111 7.50019L24.3801 25.8099" stroke="#515151" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M12.7437 19.4462L12.5993 3.79296C12.5929 3.10104 12.8857 2.43478 13.4133 1.94074C13.9409 1.44671 14.6601 1.16537 15.4126 1.15861C16.1651 1.15186 16.8894 1.42024 17.426 1.90472C17.9627 2.3892 18.2677 3.0501 18.2741 3.74201L18.4186 19.3953" stroke="#515151" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M1.22686 26.0091C1.31591 35.6588 7.13772 40.4236 15.0816 40.3523C23.0255 40.2809 26.219 36.9971 28.0139 33.0024L32.6629 21.1236C33.2504 19.6345 32.9651 18.261 31.5583 17.5495C30.1506 16.8371 28.3134 17.2268 27.5673 18.5454L24.3363 25.8016" stroke="#515151" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
      </svg>
      <a>Question</a>
      </div>
  )

  const renderButtonAudio = () => (
    <div className="d-flex mr-3 flex-column align-items-center justify-content-between">
      <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M12 1C14.1025 1 15.8263 2.62245 15.9877 4.68354L14 6.6712V5C14 3.897 13.103 3 12 3C10.897 3 10 3.897 10 5V10.6712L8.25716 12.414C8.09096 11.9744 8 11.4978 8 11V5C8 2.791 9.791 1 12 1ZM10.8582 12.6414L14 9.49963L16 7.49963L21.6712 1.82843L23 3.15722L16 10.1572L11.2311 14.9262L9.64049 16.5167L8.14547 18.0117L2.3288 23.8284L1 22.4996L6.59855 16.9011L8.01624 15.4834L9.43243 14.0672L10.8582 12.6414ZM6 11C6 12.0572 6.27469 13.0513 6.75645 13.9148L5.29931 15.3719C4.47768 14.1152 4 12.6134 4 11V9H6V11ZM11 18.9381C10.7258 18.9039 10.4559 18.8558 10.1911 18.7946L11.9857 17L12 17C15.309 17 18 14.309 18 11V10.9856L19.9856 9H20V11C20 15.0793 16.9461 18.4459 13 18.9381V22H11V18.9381Z"
        style={{fill: "rgb(214, 56, 56)", transform: "scale(1.5)"}}/>
      </svg>
      <a style={{ color: "rgb(214, 56, 56)" }}>Audio</a>
    </div>
  )

  const renderButtonInvite = () => (
    <div className="d-flex mr-3 flex-column align-items-center justify-content-between">
      <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M9.602 3.7C8.448 5.637 8.967 8.927 11.026 12.725C11.956 14.437 11.723 15.745 11.364 16.54C10.382 18.718 7.689 19.339 4.839 19.996C2.875 20.45 3 20.866 3 24H1.005L1 22.759C1 20.239 1.199 18.784 4.178 18.096C7.543 17.319 10.866 16.623 9.268 13.678C4.535 4.949 7.918 0 13 0C16.321 0 18.97 2.117 18.97 6.167C18.97 9.722 17.021 13 16.587 14H14.472C14.864 12.464 16.971 9.634 16.971 6.158C16.971 1.005 11.104 1.173 9.602 3.7ZM23 19H20V16H18V19H15V21H18V24H20V21H23V19Z"
              style={{fill: "#666", transform: "scale(1.5)"}}/>
      </svg>
      <a>Invite</a>
    </div>
  )

  const renderButtonVideo = () => (
    <div className="d-flex flex-column align-items-center justify-content-between">
      <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M5 7H15C16.1046 7 17 7.89543 17 9V10.223V13.777V15C17 16.1046 16.1046 17 15 17H5C3.89543 17 3 16.1046 3 15V9C3 7.89543 3.89543 7 5 7ZM18.9959 15.1818C18.9009 17.3066 17.1482 19 15 19H5C2.79086 19 1 17.2091 1 15V9C1 6.79086 2.79086 5 5 5H15C17.1482 5 18.9009 6.69343 18.9959 8.81819L23 6V18L18.9959 15.1818ZM19 12.739L21 14.147V9.854L19 11.261V12.739Z"
          style={{fill: "#666", transform: "scale(1.5)"}}/>
      </svg>
      <a>Video</a>
    </div>
  )

  const renderButtonOther = () => (
    <div className="d-flex mr-3 flex-column align-items-center justify-content-between">
      <svg width="40" height="40" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M12 26.875C13.864 26.875 15.375 25.364 15.375 23.5C15.375 21.636 13.864 20.125 12 20.125C10.136 20.125 8.625 21.636 8.625 23.5C8.625 25.364 10.136 26.875 12 26.875Z" fill="#515151"/>
        <path d="M23.5 26.875C25.364 26.875 26.875 25.364 26.875 23.5C26.875 21.636 25.364 20.125 23.5 20.125C21.636 20.125 20.125 21.636 20.125 23.5C20.125 25.364 21.636 26.875 23.5 26.875Z" fill="#515151"/>
        <path d="M35 26.875C36.864 26.875 38.375 25.364 38.375 23.5C38.375 21.636 36.864 20.125 35 20.125C33.136 20.125 31.625 21.636 31.625 23.5C31.625 25.364 33.136 26.875 35 26.875Z" fill="#515151"/>
      </svg>
      <a>Other</a>
    </div>
  )

  const renderButtonLeave = () => (
    <div className="d-flex mr-2 flex-column align-items-center justify-content-between">
      <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M3 5L3 19C3 20.1046 3.89543 21 5 21H13C14.1046 21 15 20.1046 15 19L15 15H17V19C17 21.2091 15.2091 23 13 23H5C2.79086 23 1 21.2091 1 19V5C1 2.79086 2.79086 1 5 1H13C15.2091 1 17 2.79086 17 5V9H15V5C15 3.89543 14.1046 3 13 3L5 3C3.89543 3 3 3.89543 3 5ZM22.5858 10.5858L19.7574 7.75735L18.3431 9.17157L20.1716 11L9 11L9 13L20.1716 13L18.3431 14.8284L19.7574 16.2426L22.5858 13.4142L23 13L24 12L23 11L22.5858 10.5858Z"
          style={{fill: "#666", transform: "scale(1.5)"}}/>
      </svg>
      <a>Leave</a>
    </div>
  )

  return (
    <>
      <div id="toolbar" className="d-flex p-3 position-absolute px-5 align-items-center justify-content-between">
      <div className="d-flex">
        {renderButtonShareScreen()}
        {renderButtonMessage()}
        {renderButtonQuestion()}
      </div>
      <div className="d-flex">
        {renderButtonAudio()}
        {renderButtonInvite()}
        {renderButtonVideo()}
      </div>
      <div className="d-flex">
          {renderButtonOther()}
          {renderButtonLeave()}
      </div>
      </div>
    </>
  )
}
