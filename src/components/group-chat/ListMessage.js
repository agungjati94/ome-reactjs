import React from 'react';

export const ListMessage = () => {

  return (
    <div className="flex-grow-1 flex-column p-2">
      <div className="d-flex mt-2">
        <div className="avatar mr-2"></div>
        <div className="d-flex flex-column">
          <b>Anne William</b>
          <p className="text">
            Kurang jelas suaranya
          </p>
          <small className="time">13:03</small>
        </div>
      </div>

      <div className="d-flex mt-2">
        <div className="avatar mr-2"></div>
        <div className="d-flex flex-column">
          <b>Anne William</b>
          <p className="text">
            Duh
          </p>
          <small className="time">13:03</small>
        </div>
      </div>

      <div className="d-flex mt-2 justify-content-end">
        <div className="d-flex flex-column">
          <b className="text-right">Rafatar</b>
          <p className="text">
            Kedengeran kok
          </p>
          <small className="time">13:03</small>
        </div>
        <div className="avatar ml-2"></div>
      </div>

    </div>
  )
}
