import React, {useState, useEffect} from 'react';
import './App.css';
import { Toolbar } from './components/toolbar/Toolbar';
import { GroupChat } from './components/group-chat/GroupChat';

function App() {

  return (<div className="position-relative full-container d-flex align-items-center justify-content-center">
    <div id="full-screen" className="h-100 w-100 d-flex">
      <video className="flex-grow-1" controls>
        <source src="movie.mp4" type="video/mp4" />
        <source src="movie.ogg" type="video/ogg" />
      Your browser does not support the video tag.
      </video>

      <GroupChat />
    </div>
     <Toolbar />
  </div>);
}

export default App;
